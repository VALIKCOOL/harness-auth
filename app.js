// ------------------------------------------------------------
// Required Core packages
// ------------------------------------------------------------
const express = require("express");
const path = require("path");
const passport = require("passport");
const session = require("express-session");
const logger = require("morgan");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const router = require("./server/routes");
const config = require("./server/config/main");
const { getAppToken } = require("./server/external/yodlee");

// ------------------------------------------------------------
// Database initialize
// ------------------------------------------------------------
mongoose.Promise = global.Promise;
mongoose.connect(config.database, { useMongoClient: true });

// ------------------------------------------------------------
// App Creation & Configure
// ------------------------------------------------------------
const app = express();

app.use(express.static(path.join(__dirname, "server/public")));

// ------------------------------------------------------------
// Middleware
// ------------------------------------------------------------
// Set 'views' directory for any views
// being rendered res.render()
app.set("views", path.join(__dirname, "server", "views"));

// Set view engine as EJS
app.set("view engine", "ejs");

if (process.env.NODE_ENV !== "production") {
    app.use(logger("dev"));
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cookieParser());
app.use(session({
    secret: "supersecret"
}));
app.use(passport.initialize());
app.use(passport.session());

// enable CORS from client-side
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Max-Age", "100000");
    next();
});

// ------------------------------------------------------------
// Routing
// ------------------------------------------------------------

router(app);


app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.elocals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page if needed
    res.status(err.status || 500);
    return res.status(400).json(err);
});

app.get("*", (req, res) => {
    res.status(404).send();
});

module.exports = app;
