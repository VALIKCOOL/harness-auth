const redisAsync = require("async-redis");
var redisClustr = require('redis-clustr');
const { host, port } = require("./server/config/redis");

let connectClient = () => {
    console.log("Redis port:", port);
    console.log("Redis host:", host);

    // var redis = new redisClustr({
    //     servers: [
    //         {
    //             host:"rediscluster.rrbrel.clustercfg.use2.cache.amazonaws.com",
    //             port,
    //         }
    //     ],
    //     createClient: function (port, host) {
    //         // this is the default behaviour
    //         return redisAsync.createClient(port, host);
    //     }
    // });
    const redis = redisAsync.createClient({ port, host });

    redis.on('connect', function () {
        console.log(`Connected to Redis`);
    });

    redis.on('error', function (err) {
        console.log('Redis error: ' + err);
    });

    return redis;
};

let client = connectClient();

module.exports.redisClient = client;
