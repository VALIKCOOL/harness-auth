const NodeRSA = require("node-rsa");
const key = new NodeRSA();
const fs = require("fs");
const path = require("path");

const publicKey = fs.readFileSync(path.join(__dirname, "keys", "rsa", "publicKey.dat"), "utf8");
const privateKey = fs.readFileSync(path.join(__dirname, "keys", "rsa", "privateKey.dat"), "utf8");

key.importKey(publicKey, "pkcs8-public-pem");
key.importKey(privateKey, "pkcs8-private-pem");
console.log("is empty", key.isEmpty());
console.log("is public", key.isPublic());
// console.log("is private", key.isPrivate());
const encryptedSecret = key.encrypt("string secret", "base64");
console.log("Encrypted", encryptedSecret);
const decryptedSecret = key.decrypt(encryptedSecret, "utf8");

console.log(decryptedSecret);
