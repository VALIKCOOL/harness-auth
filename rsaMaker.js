const NodeRSA = require("node-rsa");
const key = new NodeRSA();
const fs = require("fs");
const path = require("path");

key.generateKeyPair([2048]);

const publicKey = key.exportKey("pkcs8-public-pem");
const privateKey = key.exportKey("pkcs8-private-pem");

fs.writeFileSync(path.join(__dirname, "keys", "rsa", "publicKey.dat"), publicKey);
fs.writeFileSync(path.join(__dirname, "keys", "rsa", "privateKey.dat"), privateKey);
