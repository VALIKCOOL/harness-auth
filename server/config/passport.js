// =============================================
// Import Packages
// =============================================

const passport = require("passport");
const config = require("./main");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const LocalStrategy = require("passport-local").Strategy;
const {
    User
} = require("../dto/schema");

const localOptions = {
    usernameField: "email",
    passwordField: "password",
    passReqToCallback: true
};

const localLogin = new LocalStrategy(localOptions, async (req, email, password, done) => {
    try {
        const user = await User.findOne({
            email: email.toLowerCase(),
        });
        if (!user) {
            return done(null, false, {
                error: "Please check your credentials and try again."
            });
        }

        const passwordCheck = await user.comparePassword(password);
        if (!passwordCheck) {
            return done(null, false, {
                error: "Please check your credentials and try again."
            });
        }

        return done(null, user);
    } catch (err) {
        return done(err);
    }
});

// =============================================
// JWT strategy
// =============================================

/*
 jwtOptions Object.
 - contains options for jwt strategy
 */

const jwtOptions = {
    // Telling passport to check authorization header for JWT token
    jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderAsBearerToken(),
        ExtractJwt.fromUrlQueryParameter("jwt")]
    ),
    // Telling passport where to find secret
    secretOrKey: config.secret
};

/*
 jwtLogin().
 - strategy for JWT Login
 - contains payload which has user credentials
 - if id found, then returns the user related to that id
 */
const jwtLogin = new JwtStrategy(jwtOptions, async (payload, done) => {
    try {
        const user = await User.findById(payload.id);
        if (user) {
            done(null, user);
        } else {
            done(null, false);
        }
    } catch (err) {
        done(err, false);
    }
});

passport.use(localLogin);
passport.use(jwtLogin);

passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(async function (id, done) {
    try {
        const user = await User.findById(id).populate("instance");
        if (user) {
            done(null, user);
        }
    }
    catch (err) {
        done(err, null);
    }
});