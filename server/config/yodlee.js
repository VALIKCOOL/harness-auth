module.exports = {
    // issuerId
    iss: process.env.YODLEE_ISSUER_ID || "4500842f-23f8-4f0d-a303-dd6b976dfef4",
    baseUrl: process.env.YODLEE_BASE_URL || "https://development.api.yodlee.com/ysl/",
};
