const BASE_URL = process.env.BASE_URL || "http://api.harnesstogether.com";

exports.BASE_URL = BASE_URL;