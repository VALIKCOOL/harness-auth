const SUPER_ADMIN = "SUPER_ADMIN";
const ADMIN = "ADMIN";
const USER = "USER";

const USER_TYPES = [
    SUPER_ADMIN,
    ADMIN,
    USER
];

exports.USER_TYPES = USER_TYPES;
exports.SUPER_ADMIN = SUPER_ADMIN;
exports.ADMIN = ADMIN;
exports.USER = USER;
