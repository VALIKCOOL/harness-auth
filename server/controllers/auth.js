const jwt = require("jsonwebtoken");
const config = require("../config/main");
const {
    GetRootDomainName
} = require("../helpers/domain");
const {
    WITHDRAWN,
    SUPER_ADMIN,
} = require("../constants/userTypes");

const {
    User,
    Instance,
    PasswordRequest,
    EmailConfirmations
} = require("../dto/schema");

const randtoken = require("rand-token");

const {
    sendConfirmationEmail,
    sendPasswordRequestEmail
} = require("../services/emailService");

const {
    verifyEmailConfirmation,
    verifyPasswordRequest,
} = require("./user");

const {
    registerYodleeUser,
} = require("../services/yodleeService");

const generateToken = (user) => {
    return jwt.sign(user, config.secret, {
        expiresIn: "7d"
    });
};

const getUserSession = ({
    user
}, res) => {
    res.status(200).json({
        user: {
            _id: user._id,
            email: user.email,
            instance: user.instance ? user.instance : null,
            superAdmin: user.superAdmin,
            userImageUrl: user.userImageUrl,
            permissions: user.permissions,
            creationDate: user.creationDate,
            profile: user.profile,
            verified: user.verified,
            yodleeUserId: user.yodleeUserId,
        }
    });
};

const login = ({ user }, res) => {
    try {
        // Cookies must be formatted with leading dot and root domain name without subdomains
        const token = generateToken({
            id: user.id
        });
        return res.status(200).json({
            user,
            token
        });
    } catch (err) {
        return res.status(403).send({
            error: err.message
        });
    }
};

const loginSU = ({
    user,
    hostname
}, res) => {
    if (user && user.permissions.role === SUPER_ADMIN) {
        res.cookie("token", generateToken({
            id: user.id
        }), {
                domain: `.${GetRootDomainName(hostname)}`
            });
        res.status(200).json({
            user
        });
    } else {
        res.clearCookie("token", {
            domain: `.${GetRootDomainName(hostname)}`
        });
        res.status(401).send();
    }
};

const signup = async ({
    body
}, res) => {
    try {
        const email = body.email.toLowerCase();
        const password = body.password;

        if (!email) {
            return res.status(400).send({
                error: "You must enter an email address."
            });
        }

        if (!password) {
            return res.status(400).send({
                error: "You must enter password."
            });
        }

        const user = await User.findOne({
            email
        });

        if (user) {
            throw new Error("User already exists.");
        }

        if (!user) {
            const yodleeUser = await registerYodleeUser({ email });
            const user = new User({
                email,
                password,
                yodleeUserId: yodleeUser.id,
            });

            await user.save();
            await sendVerifyEmail(user);

            return res.status(200).json({
                user
            });
        }
        throw new Error("Sign up cannot be performed.");
    } catch (err) {
        return res.status(400).send({
            error: err.message
        });
    }
};

const sendVerifyEmail = async (user) => {
    const token = randtoken.generate(16);
    const confirmation = new EmailConfirmations({
        token,
        email: user.email,
        user: user._id
    });
    await confirmation.save();
    await sendConfirmationEmail(user.email, token);
}

const validateEmail = async ({
    query
}, res) => {
    try {
        const email = query.email.toLowerCase();

        if (!email) {
            throw new Error("You must enter an email address");
        }

        const user = await User.findOne({
            email
        });
        if (user) {
            throw new Error("User already exists");
        }
        if (!user) {
            return res.status(200).end();
        }
        throw new Error("Email already taken");
    } catch (err) {
        return res.status(400).send({
            error: err.message
        });
    }
};

const resetPasswordRequest = async ({ body }, res) => {
    try {
        const email = body.email.toLowerCase();
        if (!email) {
            throw new Error("Email is not provided");
        }
        const user = await User.findOne({
            email
        });

        if (!user) {
            throw new Error("This is an error message for the user such as email doesn’t exisit");
        }

        var token = randtoken.generate(16);
        const passwordRequest = new PasswordRequest({
            token,
            userID: user._id
        });
        await passwordRequest.save();
        await sendPasswordRequestEmail(email, token);
        return res.status(200).end();
    }
    catch (err) {
        res.status(400).json({
            error: err.message
        });
    }
};

const resetPassword = async (req, res) => {
    try {
        const token = req.params.token;
        const passwordRequest = await verifyPasswordRequest(token);
        if (passwordRequest && passwordRequest.active) {
            return res.render("resetPassword", { token, error: "", });
        }
        else {
            return res.render("resetPasswordResult", { result: " Your request no more valid" });
        }
    } catch (err) {
        return res.render("resetPasswordResult", { result: " Your request no more valid" });
    }
};

const changePassword = async ({
    body,
    user
}, res) => {
    try {
        const currentPassword = body.currentPassword;
        const newPassword = body.newPassword;
        const confirmNewPassword = body.confirmNewPassword;

        if (!currentPassword) {
            throw new Error("You must enter current password");
        }

        if (currentPassword.length < 6) {
            throw new Error("Password is wrong");
        }

        if (!newPassword) {
            throw new Error("You must enter new password");
        }

        if (newPassword.length < 6) {
            throw new Error("Password is too weak");
        }

        if (!confirmNewPassword) {
            throw new Error("You must enter confirmation password");
        }

        if (confirmNewPassword.length < 6) {
            throw new Error("Password is too weak");
        }

        if (confirmNewPassword !== newPassword) {
            throw new Error("Passwords do not match");
        }

        const compareResult = await user.comparePassword(currentPassword);
        if (!compareResult) {
            throw new Error("Password is wrong");
        } else {
            if (confirmNewPassword === currentPassword) {
                throw new Error("You can't set previous password");
            }
            user.password = confirmNewPassword;
            await user.save();
            return res.status(200).send();
        }
    } catch (err) {
        return res.status(400).send({
            error: err.message
        });
    }
};

const validatePasswordString = password =>
    // eslint-disable-next-line implicit-arrow-linebreak
    password.length > 7 && /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/.test(password);

const changeForgottenPassword = async ({
    body
}, res) => {
    try {
        const newPassword = body.password;
        const confirmNewPassword = body.confirmPassword;
        const token = body.token;

        if (!newPassword) {
            return res.render("resetPassword", {
                token,
                error: "You must enter new password"
            });
        }

        if (!validatePasswordString(newPassword)) {
            return res.render("resetPassword", {
                token,
                error: "Your password needs at least 8 characters and contain both letters and numbers"
            });
        }

        if (!confirmNewPassword) {
            return res.render("resetPassword", {
                token,
                error: "You must enter confirm password"
            });
        }

        if (!validatePasswordString(confirmNewPassword)) {
            return res.render("resetPassword", {
                token,
                error: "Your password needs at least 8 characters and contain both letters and numbers"
            });
        }

        if (confirmNewPassword !== newPassword) {
            return res.render("resetPassword", {
                token,
                error: "Passwords do not match"
            });
        }

        const passwordRequest = await PasswordRequest.findOne({
            token,
            active: true
        });

        if (!passwordRequest || passwordRequest.active === false) {
            return res.render("resetPasswordResult", {
                result: " Your request no more valid"
            });
        }

        const user = await User.findOne({
            _id: passwordRequest.userID,
        });

        if (!user) {
            return res.render("resetPasswordResult", {
                result: " Your request no more valid"
            });
        }

        user.password = confirmNewPassword;
        passwordRequest.active = false;
        await passwordRequest.save();
        await user.save();

        return res.render("resetPasswordResult", { result: "Password changed successfully." });
    } catch (err) {
        return res.render("resetPasswordResult", {
            result: err.message,
        });
    }
};

const changeEmail = async ({
    subdomains,
    body,
    user
}, res) => {
    try {
        const password = body.password;
        const email = body.email.toLowerCase();
        const newEmail = body.newEmail;

        if (!password) {
            throw new Error("You must enter current password");
        }

        if (password.length < 6) {
            throw new Error("Password is wrong");
        }

        if (!email) {
            throw new Error("You must enter email");
        }

        if (!newEmail) {
            throw new Error("You must enter new email");
        }

        if (email === newEmail) {
            throw new Error("You can't set previous email");
        }

        const compareResult = await user.comparePassword(password);
        if (!compareResult) {
            throw new Error("Password is wrong");
        } else {
            if (user.permissions.role !== SUPER_ADMIN) {
                const instance = await Instance.findOne({
                    url: subdomains[0]
                });
                const lookUpUser = await User.findOne({
                    email: newEmail,
                    instance: instance._id
                });
                if (lookUpUser) {
                    throw new Error("User with specified email already signed up into this community");
                }
            } else {
                const lookUpUser = await User.findOne({
                    email: newEmail,
                    "permissions.role": SUPER_ADMIN
                });
                if (lookUpUser) {
                    throw new Error("User with specified email already signed up");
                }
            }
            await EmailConfirmations.updateMany({
                user: user._id
            }, {
                    active: false
                });
            const token = randtoken.generate(16);
            const confirmation = new EmailConfirmations({
                token,
                currentEmail: email,
                newEmail,
                user: user._id
            });
            await confirmation.save();
            await sendConfirmationEmail(newEmail, token);
        }
        return res.status(200).send();
    } catch (err) {
        return res.status(422).send({
            error: err.message
        });
    }
};

const roleAuthorization = (roles) => {
    return ({
        user
    }, res, next) => {
        try {
            if (!user) {
                throw new Error("Unauthorized");
            }
            if (user.superAdmin === true) {
                return next();
            }

            const permissions = user.permissions;
            if (roles.includes(permissions.role)) {
                return next();
            }
            res.status(401).json({
                error: "You are not authorized to view this content."
            });
        } catch (error) {
            res.status(422).json({
                error: error.message
            });
            return next(error);
        }
    };
};

const checkPermissions = (predict, view) => {
    return ({
        user
    }, res, next) => {
        try {
            if (!user) {
                throw new Error("Unauthorized");
            }
            if (user.superAdmin === true) {
                return next();
            }
            let allowed = false;

            if (predict !== undefined) {
                allowed = user.permissions.predict === predict;
                if (!allowed) {
                    throw new Error("You do not have permissions.");
                }
            }
            if (view !== undefined) {
                allowed = user.permissions.view === view;
                if (!allowed) {
                    throw new Error("You do not have permissions.");
                }
            }

            if (allowed) {
                return next();
            }
            throw new Error("You do not have permissions.");
        } catch (err) {
            res.status(401).json({
                error: err.message
            });
            return next(err);
        }
    };
};

const checkWithdrawn = ({
    user,
    hostname
}, res, next) => {
    try {
        if (!user) {
            res.clearCookie("token", {
                domain: `.${GetRootDomainName(hostname)}`
            });

            throw new Error("Unauthorized");
        }
        if (user.superAdmin === true) {
            return next();
        }
        if (user.status !== WITHDRAWN) {
            return next();
        }
        res.clearCookie("token", {
            domain: `.${GetRootDomainName(hostname)}`
        });
        throw new Error("You do not have permissions.");
    } catch (err) {
        res.status(401).json({
            error: err.message
        });
        return next(err);
    }
};

const verifyUser = async (req, res) => {
    try {
        const confirmation = await verifyEmailConfirmation(req.params.token);
        if (confirmation && confirmation.active) {
            const user = await User.findById(confirmation.user);
            if (!user) {
                throw new Error("You confirmation no longer valid");
            }
            user.verified = true;
            await user.save();
            confirmation.active = false;
            await confirmation.save();
            return res.render('verification', { email: user.email });
        }
        throw new Error("You confirmation no longer valid");
    } catch (err) {
        res.status(401).json({
            error: err.message
        });
    }
};

exports.getUserSession = getUserSession;
exports.login = login;
exports.loginSU = loginSU;
exports.signup = signup;
exports.validateEmail = validateEmail;
exports.changePassword = changePassword;
exports.changeForgottenPassword = changeForgottenPassword;
exports.changeEmail = changeEmail;
exports.roleAuthorization = roleAuthorization;
exports.checkPermissions = checkPermissions;
exports.checkWithdrawn = checkWithdrawn;
exports.verifyUser = verifyUser;
exports.resetPasswordRequest = resetPasswordRequest;
exports.resetPassword = resetPassword;