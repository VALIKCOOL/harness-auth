const mongoose = require("mongoose");
const multer = require("multer");

const {
    User,
    EmailConfirmations,
    Invitation,
    PasswordRequest,
} = require("../dto/schema");

const moment = require("moment");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "./wwwroot/assets/uploads");
    },
    filename: (req, file, cb) => {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + "-" + datetimestamp + "." + file.originalname.split(".")[file.originalname.split(".").length - 1]);
    }
});

const upload = multer({
    storage
}).array();

const createUser = async (email, password, profile, permissions = {}) => {
    try {
        const user = new User({
            email,
            password,
            profile,
            instance: permissions.role !== "SUPER_ADMIN" ? mongoose.Types.ObjectId("5966610fc31adb385c89a8b0") : null,
            permissions,
            superAdmin: permissions.role === "SUPER_ADMIN" ? true : false
        });

        await user.save();
    } catch (err) {
        return err;
    }
};

// createUser("sample@admin.com", "qwerty", {firstName: "Ivan", lastName: "Ivanov"}, {role: "SUPER_ADMIN"});

const updateUser = (_id, updateObject) => {
    try {
        return User.findByIdAndUpdate(
            mongoose.Types.ObjectId(_id), updateObject);
    } catch (err) {
        return err;
    }
};

const updateManyUsers = (ids, updateObject) => {
    try {
        return User.updateMany({
            _id: {
                $in: ids
            }
        }, updateObject);
    } catch (err) {
        return err;
    }
};

const verifyInvitation = async (token) => {
    const invite = await Invitation.findOne({
        token
    }).populate("instance");
    if (invite) {
        const validityDate = moment(invite.createdAt).add(2, "weeks");
        if (moment().isAfter(validityDate)) {
            throw new Error("Invitation no more valid");
        }
    }
    return invite;
};

const verifyPasswordRequest = async (token) => {
    const passwordRequest = await PasswordRequest.findOne({
        token
    });
    if (passwordRequest) {
        const validityDate = moment(passwordRequest.createdAt).add(2, "weeks");
        if (moment().isAfter(validityDate)) {
            throw new Error("Request no more valid");
        }
    }
    return passwordRequest;
};

const verifyEmailConfirmation = async (token) => {
    const confirmation = await EmailConfirmations.findOne({
        token
    });
    if (confirmation) {
        const validityDate = moment(confirmation.createdAt).add(2, "weeks");
        if (moment().isAfter(validityDate)) {
            throw new Error("Invitation no more valid");
        }
    }
    return confirmation;
};

const setUserImage = (req, res) => {
    const file = req.file;
    return upload(req, res, (err) => {
        if (file) {
            const fileName = file.filename;
            req.user.userImageUrl = "/assets/uploads/" + fileName;
            if (err) {
                return Promise.reject(err);
            }
            return req.user.save();
        } else {
            return Promise.reject("No file presented");
        }
    });
};

exports.createUser = createUser;
exports.updateUser = updateUser;
exports.updateManyUsers = updateManyUsers;
exports.verifyInvitation = verifyInvitation;
exports.verifyPasswordRequest = verifyPasswordRequest;
exports.verifyEmailConfirmation = verifyEmailConfirmation;
exports.setUserImage = setUserImage;
