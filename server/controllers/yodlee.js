const {
    getToken,
    shouldLinkAccount,
    territories,
    countryDetails,
    allTransactions,
} = require("../services/yodleeService");

const link = async ({ user }, res) => {
    const token = await getToken(user.email);
    return res.render('fastlink', { title: "Link Account", token });
};

const shouldLink = async ({ user }, res) => {
    try {
        const result = await shouldLinkAccount(user.email);
        return res.status(200).json(result);
    }
    catch (err) {
        throw new Error(err.message);
    }
};

const getTerritories = async ({ user }, res) => {
    try {
        const result = await territories(user.email);
        return res.status(200).json(result);
    }
    catch (err) {
        throw new Error(err.message);
    }
}

const getCountryDetails = async ({ user, query }, res) => {
    try {
        const result = await countryDetails(user.email, query.countryCode);
        return res.status(200).json(result);
    }
    catch (err) {
        throw new Error(err.message);
    }
};

const getAllTransactions = async ({ user, query }, res) => {
    try {
        const result = await allTransactions(
            user.email,
            query.fromDate,
            query.take,
            query.skip,
        );
        return res.status(200).json(result);
    }
    catch (err) {
        throw new Error(err.message);
    }
};

exports.link = link;
exports.shouldLink = shouldLink;
exports.getTerritories = getTerritories;
exports.getCountryDetails = getCountryDetails;
exports.getAllTransactions = getAllTransactions;
