const mongoose = require("mongoose");

const emailConfirmationSchema = new mongoose.Schema({
    token: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: "user"
    },
    active: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true,
    collection: "emailConfirmations"
});

exports.emailConfirmationSchema = emailConfirmationSchema;
