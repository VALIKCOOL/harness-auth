const mongoose = require("mongoose");

const passwordRequestSchema = new mongoose.Schema({
    token: {
        type: String,
        unique: true,
        required: true
    },
    active: {
        type: Boolean,
        default: true
    },
    userID: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: "user"
    }
}, {
    timestamps: true,
    collection: "passwordRequests"
});

exports.passwordRequestSchema = passwordRequestSchema;
