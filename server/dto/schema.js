const mongoose = require("mongoose");

const {
    userSchema
} = require("./userSchema");
const {
    emailConfirmationSchema
} = require("./emailConfirmationSchema");
const {
    passwordRequestSchema
} = require("./passwordRequestSchema");

exports.PasswordRequest = mongoose.model("passwordRequest", passwordRequestSchema);
exports.User = mongoose.model("user", userSchema);
exports.EmailConfirmations = mongoose.model("emailConfirmation", emailConfirmationSchema);
