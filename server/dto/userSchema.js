const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const {
    USER_TYPES,
    USER
} = require("../constants/userTypes");

const SALT_FACTOR = 5;

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    profile: {
        firstName: {
            type: String,
        },
        lastName: {
            type: String
        }
    },
    creationDate: {
        type: Date,
        required: true,
        default: Date.now
    },
    permissions: {
        role: {
            type: String,
            enum: USER_TYPES,
            default: USER
        },
    },
    verified: {
        type: Boolean,
        default: false
    },
    yodleeUserId: {
        type: Number,
    },
}, {
        timestamps: true,
        collection: "users"
    });

userSchema.pre("save", function (next) {
    const user = this;

    if (!user.isModified("password")) return next();

    bcrypt.hash(user.password, SALT_FACTOR).then((hash) => {
        user.password = hash;
        next();
    });
});

userSchema.methods.comparePassword = function (candidatePassword) {
    return bcrypt.compare(candidatePassword, this.password);
};

exports.userSchema = userSchema;
