const {
    iss
} = require("../config/yodlee");
const path = require("path");
const fs = require("fs");

let jwt = require("jsonwebtoken");

function getAppToken(user) {
    let privateKey = fs.readFileSync(path.join(__dirname, "../..", "keys", "rsa", "privateKey.dat"),
        "utf8"); // Location of the file with your private key
    let currentTime = Math.floor(Date.now() / 1000);
    let signOptions = {
        algorithm: "RS512" // Yodlee requires RS512 algorithm when encoding JWT
    };

    const payload = {
        iss, // The issuer id from the API Dashboard
        iat: currentTime, // Epoch time when token is issued in seconds
        exp: currentTime + 900 // Epoch time when token is set to expire. Must be 900 seconds
    };

    if (user) {
        payload.sub = `harness_${user}`;
    }

    let token = jwt.sign(payload, privateKey, signOptions);
    return token;
}

function checkTokenExpiration(token) {
    const { exp } = jwt.decode(token);
    if (Date.now() / 1000 > exp) {
        return false;
    }
    return true;
}

module.exports = {
    getAppToken,
    checkTokenExpiration,
};
