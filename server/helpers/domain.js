const IsSubdomain = (hostname) => {
    return !!hostname.match(new RegExp(/^([a-z]+\:\/{2})?([\w-]+\.[\w-]+\.\w+)$/));
};

const GetRootDomainName = (hostname) => {
    return IsSubdomain(hostname) ? hostname.replace(/.+?(?=\.)\./, "") : hostname;
};

exports.IsSubdomain = IsSubdomain;
exports.GetRootDomainName = GetRootDomainName;