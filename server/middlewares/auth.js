const passportService = require("../config/passport"); // Note: this is never used manually here;
const passport = require("passport"); // passport will automatically use the passportService

const adminLogin = (req, res, next) => {
    req.superAdmin = true;
    next();
};

const requireAuth = passport.authenticate("jwt", {
    session: false
});

const requireLogin = (req, res, next) => {
    passport.authenticate("local", {
        session: false
    }, (err, user, info) => {    
        if(err || !user) {
            return res.status(400).json(info);
        } 
        req.user = user;
        return next(); // continue to next middleware if no error.
    })(req, res, next);
};

exports.adminLogin = adminLogin;
exports.requireAuth = requireAuth;
exports.requireLogin = requireLogin;