const { getToken } = require("../services/yodleeService");

const yodleeMiddlware = (req, res, next) => {
    req.jwt = getToken();
    next();
};

exports.yodleeMiddlware = yodleeMiddlware;