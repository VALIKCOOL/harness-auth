const express = require("express");
const router = express.Router();

const yodlee = require("./yodlee");
const user = require("./user");

router.use("/yodlee", yodlee);

router.use("/user", user);

module.exports = router;