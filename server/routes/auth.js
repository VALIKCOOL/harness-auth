const {
    changePassword,
    changeForgottenPassword,
    changeEmail,
    signup,
    validateEmail,
    login,
    loginSU,
    checkWithdrawn,
    getUserSession,
    verifyUser,
    resetPasswordRequest,
    resetPassword,
} = require("../controllers/auth");
const express = require("express");
const router = express.Router();

const {
    adminLogin,
    requireAuth,
    requireLogin
} = require("../middlewares/auth");

router.get("/session", requireAuth, getUserSession);
router.post("/changePassword", requireAuth, changePassword);
router.post("/changeForgottenPassword", changeForgottenPassword);
router.post("/changeEmail", requireAuth, changeEmail);
router.post("/signup", signup);
router.get("/validateEmail", validateEmail);
router.post("/login", [requireLogin], login);
router.post("/loginAdmin", [adminLogin, requireLogin], loginSU);
router.get("/verifyUserConfirmation/:token", verifyUser);
router.post("/resetPasswordRequest", resetPasswordRequest);
router.get("/resetPassword/:token", resetPassword);

module.exports = router;
