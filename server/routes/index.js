const {
    requireAuth
} = require("../middlewares/auth");

const auth = require("./auth");
const api = require("./api");

module.exports = app => {
    // /auth
    app.use("/auth", auth);

    // /api
    //todo: remove requireAuth attr because fastlink api renering by WebView and we can pass auth attr
    app.use("/api", requireAuth, api);
};
