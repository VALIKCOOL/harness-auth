const express = require("express");
const router = express.Router();
const randtoken = require("rand-token");

const {
    GetRootDomainName
} = require("../helpers/domain");

const {
    SUPER_ADMIN
} = require("../constants/userTypes");

const {
    verifyInstanceUrl,
    getAllInstances
} = require("../controllers/instance");

const {
    verifyInvitation,
    verifyPasswordRequest,
} = require("../controllers/user");

const {
    User,
} = require("../dto/schema");

const {
    roleAuthorization
} = require("../controllers/auth");

const {
    requireAuth
} = require("../middlewares/auth");

router.get("/verify/:instanceDomainUrl", verifyInstanceUrl);

router.get("/instances", [requireAuth, roleAuthorization([SUPER_ADMIN])], (req, res) => {
    getAllInstances((err, instances) => {
        if (err) {
            instances = {
                "error": true,
                "message": "Wrong Instance provided!"
            };
        }
        const resultInstances = instances.map((el) => {
            return {
                _id: el._id,
                name: el.name,
                url: el.url,
                createdAt: el.createdAt,
                description: el.description,
                isPublic: el.isPublic,
                creationDate: el.creationDate,
                eventsNumber: el.events.length,
                approvedUsers: el.users.length + el.admins.length
            };
        });
        res.status(200).json({
            instances: resultInstances
        });
    });
});

router.get("/activate/:token", async(req, res) => {
    try {
        const invite = await verifyInvitation(req.params.token);
        if (invite && invite.active) {
            return res.redirect(`/signUp?token=${invite.token}&instance=${invite.instance.url}&email=${invite.email}&role=${invite.permissions.role}`);
        } else throw new Error("Your invitation no longer valid");
    } catch (err) {
        res.redirect(`/error?error=${err.message}`);
    }
});

router.get("/activatePassword/:token", async(req, res) => {
    try {
        const request = await verifyPasswordRequest(req.params.token);
        if (request && request.active) {
            return res.redirect(`/changePassword?token=${request.token}`);
        } else throw new Error("Your request no longer valid");
    } catch (err) {
        res.redirect(`/error?error=${err.message}`);
    }
});

module.exports = router;
