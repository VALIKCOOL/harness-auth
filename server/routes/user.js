const express = require("express");
const router = express.Router();

const {
    createSuperAdmin,
} = require("../controllers/user");


router.get("/", ({user}, res) => {
    res.status(200).json({
        user:{
            email: user.email,
            instance: user.instance ? user.instance : null,
            superAdmin: user.superAdmin,
            userImageUrl: user.userImageUrl,
            permissions: user.permissions,
            creationDate: user.creationDate,
            profile: user.profile
        }
    });
});

router.post("/", (req, res) => {
    const email = req.body.email;
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const password = req.body.password;

    createSuperAdmin(email, password, firstName, lastName);

    res.end("USER CREATED");
});

module.exports = router;
