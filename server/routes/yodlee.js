const express = require("express");
const router = express.Router();

const {
    link,
    shouldLink,
    getTerritories,
    getCountryDetails,
    getAllTransactions,
} = require("../controllers/yodlee");

router.get("/link", link);
router.get("/shouldLink", shouldLink);
router.get("/territories", getTerritories);
router.get("/countryDetails", getCountryDetails);
router.get("/allTransactions", getAllTransactions);

module.exports = router;