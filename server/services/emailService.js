const {
    mailApiKey,
    mailDomain
} = require("../config/main");

const mailgun = require("mailgun-js")({
    apiKey: mailApiKey,
    domain: mailDomain
});

const {
    BASE_URL
} = require("../constants/apiData");

const sendAccessRequest = async (admin, requestValues, instanceUrl) => {
    var data = {
        from: "notification@system-ii.biz",
        to: admin.email,
        subject: "System II Access Email",
        html: `<h1>Dear ${admin.profile.firstName || "Admin"}, ${requestValues.fullName} is asking you to provide him an access to ${instanceUrl} community. His contact info is show below.</h1>
               <div>
                    <h2>Contact Info:</h2>
                    <h5>Email: ${requestValues.email}</h5>
                    <h5>Full Name: ${requestValues.fullName}</h5>
                    <h5>Message: ${requestValues.message}</h5>
                    <h5>Phone: ${requestValues.phone}</h5>
               </div>`
    };

    return await mailgun.messages().send(data);
};

const sendInviteEmail = async (emailID, { profile }, fullName, instanceName, token, domain) => {
    const verifyUrl = `http://${domain}/tunnel/activate/${token}`;

    var data = {
        from: "notification@system-ii.biz",
        to: emailID,
        subject: "System II Invitation Email",
        html: `<h1>Dear ${fullName || "User"}, you have been invited into community ${instanceName} by ${profile.firstName} ${profile.lastName} on SYSTEM-II portal</h1><p>Please verify your account by clicking <a href="${verifyUrl}">this link</a>.</p>`,
        text: "Please verify your account by clicking this link"
    };

    return await mailgun.messages().send(data);
};

const sendAllocatedCommunitiesEmail = async (emailID, instances) => {
    const instancesList = instances.join(", ");
    var data = {
        from: "notification@system-ii.biz",
        to: emailID,
        subject: "System II Allocated Communities List",
        html: `<h1>This email was used for membership in following communities: ${instancesList}</h1>`,
        text: `Dear user, this email was used for membership in following communities: ${instancesList}`
    };

    return await mailgun.messages().send(data);
};

const sendPasswordRequestEmail = async (email, token) => {
    const resetUrl = `${BASE_URL}/auth/resetPassword/${token}`;

    var data = {
        from: "notification@harnesstogether.com",
        to: email,
        subject: "Reset password",
        html: `<p>You can change your password by clicking <a href="${resetUrl}">this link</a>.</p>`,
        text: "You requested password reset"
    };
    try {
        return await mailgun.messages().send(data);
    }
    catch (err) {
        //todo: write exception to somewhere
        return new Promise(resolve => {
            resolve("Mailgun could not send email.");
        });
    }
};

const sendOwnCommunityRequest = async (email, name, message, referrer) => {
    var data = {
        from: "notification@system-ii.biz",
        to: "eugene@presciient.com",
        subject: "Looking to start my own community on System-II",
        html: `<h1>Hi! My name is ${name} (${email}) and I am looking to start my own community. ${referrer ? "Referred by " + referrer : ""}</h1>
                <p>${message}</p>`,
        text: "Own community"
    };

    return await mailgun.messages().send(data);
};

const sendConfirmationEmail = async (email, token) => {
    const verifyUrl = `${BASE_URL}/auth/verifyUserConfirmation/${token}`;

    var data = {
        from: `notification@harnesstogether.com`,
        to: email,
        subject: "Harness Confirmation Email",
        html: `<h1>To confirm your email please click on the following <a href="${verifyUrl}">link</a>.</p>`,
        text: "Please verify your email change by clicking this link"
    };
    try {
        return await mailgun.messages().send(data);
    }
    catch (err) {
        //todo: write exception to somewhere
        return new Promise(resolve => {
            resolve("Mailgun could not send email.");
        });
    }
};

const sendEventChangesEmail = async (emailList, eventName, adminMessage) => {
    var data = {
        from: "notification@system-ii.biz",
        to: emailList.join(),
        subject: "System II Event Changes Email",
        html: `
            <h1>Greetings!</h1>
            <p>We would like to inform you, that there were changes applied to event "${eventName}". You received this email, because you have participated in this event.</p>
            <p>Admin message:</p>
            <blockquote>
                ${adminMessage}
            </blockquote>
        `,
        text: `Some changes were applied to ${eventName}.`
    };

    return await mailgun.messages().send(data);
};

exports.sendAccessRequest = sendAccessRequest;
exports.sendInviteEmail = sendInviteEmail;
exports.sendAllocatedCommunitiesEmail = sendAllocatedCommunitiesEmail;
exports.sendPasswordRequestEmail = sendPasswordRequestEmail;
exports.sendConfirmationEmail = sendConfirmationEmail;
exports.sendOwnCommunityRequest = sendOwnCommunityRequest;
exports.sendEventChangesEmail = sendEventChangesEmail;