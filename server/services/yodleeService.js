const axios = require("axios");
const { getAppToken, checkTokenExpiration } = require("../external/yodlee");
const { baseUrl } = require("../config/yodlee");
const { redisClient } = require("../../redisClient");
const moment = require("moment");

const currencyCountry = {
    "USD": "US",
    "AUD": "AU",
    "CAD": "CA",
};

const countryCurrency = {
    "US": "USD",
    "AU": "AUD",
    "CA": "CAD",
};

const currencySymbol = {
    "US": "$",
    "AU": "$",
    "CA": "$",
};

const getToken = async (user) => {
    const key = user ? user : "appToken";
    let token = null;

    const result = await redisClient.get(key);

    token = result;

    if (token === null) {
        const newToken = getAppToken(user);
        redisClient.set(key, newToken);
        return newToken;
    } else {
        if (checkTokenExpiration(token)) {
            return token;
        } else {
            const newToken = getAppToken(user);
            redisClient.set(key, newToken);
            return newToken;
        }
    }
};

const getAxiosConfig = async (user) => {
    const token = await getToken(user);
    return {
        baseURL: baseUrl,
        headers: {
            "Content-Type": "application/json",
            "Api-Version": "1.1",
            "Authorization": `Bearer ${token}`
        }
    };
};

const registerYodleeUser = async (user) => {
    let body = {
        user: {
            loginName: `harness_${user.email}`,
            email: user.email,
            preferences: {
                locale: "en_US"
            }
        }
    };
    const config = await getAxiosConfig();
    const instance = axios.create(config);

    try {
        const result = await instance.post("/user/register/", body);
        return result.data.user;
    } catch (err) {
        if (err.response.data && err.response.data.errorMessage) {
            if (err.response.data.errorCode === "Y400") {
                throw new Error("User already exists.");
            }
            throw new Error("Sign up cannot be performed.");
        }
    }
};

const shouldLinkAccount = async (userEmail) => {
    const config = await getAxiosConfig(userEmail);
    const instance = axios.create(config);
    try {
        const { data } = await instance.get("accounts?");
        let isEmpty = !Object.keys(data).length;
        return isEmpty;
    } catch (err) {
        console.log(err);
        throw new Error("Cannot perfrom request.");
    }
};

const territories = async (userEmail) => {
    const config = await getAxiosConfig(userEmail);
    const instance = axios.create(config);
    try {
        const { data } = await instance.get("accounts?include=profile");
        if (!Object.keys(data).length) {
            return null;
        }
        const accounts = data.account;
        // accounts.push({
        //     balance: {
        //         amount: 254,
        //         currency: "AUD",
        //     }
        // });
        let result = {
            territories: {
                "US": {
                    currency: "USD",
                    symbol: "$",
                    totalBalance: 0,
                },
                "AU": {
                    currency: "AUD",
                    symbol: "$",
                    totalBalance: 0,
                },
                "CA": {
                    currency: "CAD",
                    symbol: "$",
                    totalBalance: 0,
                },
            },
            total: 0,
            //using for total balance country icon
            defaultCountryCode: null,
        };

        for (let i = 0; i < accounts.length; i++) {
            let account = accounts[i];
            if (account.balance) {
                const country = currencyCountry[account.balance.currency];
                if (country) {
                    result.territories[country].totalBalance += account.balance.amount;
                    result.total += account.balance.amount;
                    if (result.defaultCountryCode === null) {
                        result.defaultCountryCode = country;
                    }
                }
            }
        }

        return result;
    } catch (err) {
        console.log(err);
        throw new Error("Cannot perfrom request.");
    }
};

const countryDetails = async (userEmail, countryCode) => {
    const config = await getAxiosConfig(userEmail);
    const instance = axios.create(config);
    const { data } = await instance.get("accounts?");
    if (!Object.keys(data).length) {
        return null;
    }
    const accounts = data.account;
    // accounts.push({
    //     balance: {
    //         amount: 254,
    //         currency: "AUD",
    //     },
    //     accountName: "Adv Plus Banking 2299",
    //     accountType: "Cheking",
    //     container: "creditCard",
    //     providerId: 16441,
    // });
    let result = {
        totalBalance: 0,
        accounts: [],
        assets: 0,
        liabilities: 0,
        symbol: currencySymbol[countryCode],
    };
    for (let i = 0; i < accounts.length; i++) {
        let account = accounts[i];
        if (account.balance && account.balance.currency === countryCurrency[countryCode]) {
            result.totalBalance += account.balance.amount;
            account.isAsset ? result.assets += account.balance.amount : result.liabilities += 0;
            account.availableCredit ? result.liabilities += account.availableCredit.amount : result.liabilities += 0;
            const imageUrl = await getProviderIcon(userEmail, parseInt(account.providerId));
            result.accounts.push({
                balance: account.balance.amount,
                currency: account.balance.currency,
                accountName: account.accountName,
                accountType: account.accountType.replace("_", " "),
                container: account.CONTAINER,
                img: imageUrl,
            });
        }
    }
    return result;
};

const allTransactions = async (userEmail, fromDate, take, skip) => {
    const config = await getAxiosConfig(userEmail);
    const instance = axios.create(config);
    try {
        if (!fromDate || fromDate === "null" || fromDate === "undefined") {
            fromDate = moment().subtract(5, "months").format("YYYY-MM-DD");
        }
        if (skip === "undefined" || skip == "null") { skip = 0; }
        if (take === "undefined" || take == "null") { take = 10; }

        const { data } = await instance.get(`transactions?skip=${skip}&top=${take}&fromDate=${fromDate}`);
        let isEmpty = !Object.keys(data).length;
        if (isEmpty) return [];

        const transactions = data.transaction;

        const accountsId = transactions
            .map(t => t.accountId)
            .filter((elem, pos, arr) => {
                return arr.indexOf(elem) == pos;
            })
            .join(",");
        const accountsData = await instance.get(`accounts?accountId=${accountsId}`);
        const accounts = accountsData.data.account;

        const res = data.transaction.filter(t => {
            return t.categoryType === "INCOME" || t.categoryType === "EXPENSE" || t.categoryType === "TRANSFER";
        }).map(t => {
            const account = accounts.find(a => a.id === t.accountId);
            return {
                accountId: t.accountId,
                bankName: account.providerName,
                accountType: account.accountType.replace("_", " "),
                accountName: account.accountName,
                categoryType: t.categoryType,
                amount: t.amount,
                category: t.category,
                date: t.date,
                description: t.description && t.description.simple ? t.description.simple : t.description.original,
            }
        });
        // this gives an object with dates as keys
        const groups = res.reduce((groups, transaction) => {
            const date = transaction.date;
            if (!groups[date]) {
                groups[date] = [];
            }
            groups[date].push(transaction);
            return groups;
        }, {});

        let result = [];
        Object.keys(groups).map((date) => {
            result.push({
                date,
                header: true,
            });
            result = result.concat(groups[date]);
            // if (date === "Monday 25th,March")
            //     result.push(groups[date][0]);
        });

        return result;
    } catch (err) {
        console.log(err);
        throw new Error("Cannot perfrom request.");
    }
};

const getProviderIcon = async (userEmail, providerId) => {
    const redisKey = `providerImageUrl_${providerId}`;
    const result = await redisClient.get(redisKey);

    if (result === null) {
        const config = await getAxiosConfig(userEmail);
        const instance = axios.create(config);
        const { data } = await instance.get(`providers/${providerId}`);
        const imageUrl = data.provider[0].logo;
        redisClient.set(redisKey, imageUrl);

        return imageUrl;
    }
    return result;
};

module.exports = {
    registerYodleeUser,
    getToken,
    shouldLinkAccount,
    territories,
    countryDetails,
    allTransactions,
};
